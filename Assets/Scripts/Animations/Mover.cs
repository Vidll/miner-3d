using UnityEngine;

public class Mover : MonoBehaviour
{
	[SerializeField] private Transform _startPoint;
	[SerializeField] private Transform _endPoint;

	private Transform _myTransform;

	private void Start()
	{
		_myTransform = GetComponent<Transform>();
	}
	public void StartMove()
	{
		Moved();
	}

	public void StartMove(System.Action doit)
	{
		Moved();
		doit.Invoke();
	}

	private void Moved()
	{
		if (!_endPoint)
		{
			Debug.Log("Not end position");
			return;
		}
		_myTransform.position = _endPoint.position;
		_myTransform.rotation = _endPoint.rotation;
	}
}
