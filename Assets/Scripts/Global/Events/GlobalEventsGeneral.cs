public static class GlobalEventsGeneral
{
	public delegate void GameGeneralAction();

	public static GameGeneralAction GoToMine;
	public static GameGeneralAction GoToWorld;
	public static GameGeneralAction GoToWorldFromStartMenu;
	public static GameGeneralAction ChangeGameState;

	public static void GoToWorldFromStartMenuMethod()
	{
		GoToWorldFromStartMenu?.Invoke();
		ChangeGameState?.Invoke();
		GlobalParametersGeneral.CurrentGameState = GlobalParametersGeneral.GlobalGameState.OnWorld;
	}

	public static void GoToWorldMethod()
	{
		GoToWorld?.Invoke();
		ChangeGameState?.Invoke();
		GlobalParametersGeneral.CurrentGameState = GlobalParametersGeneral.GlobalGameState.OnWorld;
	}

	public static void GoToMineMethod()
	{
		GoToMine?.Invoke();
		ChangeGameState?.Invoke();
		GlobalParametersGeneral.CurrentGameState = GlobalParametersGeneral.GlobalGameState.OnMine;
	}
}
