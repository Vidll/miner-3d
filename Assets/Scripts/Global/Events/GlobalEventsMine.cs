public static class GlobalEventsMine
{
	public delegate void GameMineAction();
	public delegate void FloatMineAction(float value);

	public static GameMineAction CreateMineLine;
	public static GameMineAction DestroyMineLine;
	public static GameMineAction DestroyBlock;

	//Stuck
	public static FloatMineAction BreackOut;
	public static GameMineAction BreackOutStop;
	public static GameMineAction StuckStart;

	public static FloatMineAction ChangeDrillFuelTank;
}
