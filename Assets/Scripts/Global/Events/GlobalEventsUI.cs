public static class GlobalEventsUI 
{
	public delegate void GameUIAction();
	public delegate void GameUIActionInt(int index);
	public delegate void GameUIActionFloat(float value);

	//UI
	public static GameUIAction SetMoney;
	public static GameUIActionInt SetResouces;
	public static GameUIActionInt SetMaterials;

	//Setting
	public static GameUIAction ChangeLanguage;

	//Windows
	public static GameUIActionInt OpenWindow;
	public static GameUIActionInt SearchAndOpenWindow;
}