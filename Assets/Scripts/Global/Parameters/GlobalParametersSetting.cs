public struct SettingsParametersData
{
    public bool MusicActive;
    public bool SoundActive;
    public float MusicVolume;
    public float SoundVolume;
    public GlobalLanguage.LanguageState Language;
}

public static class GlobalParametersSetting 
{
    public static SettingsParametersData SettingsParametersData = new SettingsParametersData();

    public static bool MusicActive
    {
        get => SettingsParametersData.MusicActive;
        set
        {
            SettingsParametersData.MusicActive = value;
        }
    }

    public static bool SoundActive
    {
        get => SettingsParametersData.SoundActive;
        set
        {
            SettingsParametersData.SoundActive = value;
        }
    }

    public static float MusicVolume
    {
        get => SettingsParametersData.MusicVolume;
        set
        {
            SettingsParametersData.MusicVolume = value;
        }
    }

    public static float SoundVolume
    {
        get => SettingsParametersData.SoundVolume;
        set
        {
            SettingsParametersData.SoundVolume = value;
        }
    }

    public static GlobalLanguage.LanguageState Language
    {
        get => SettingsParametersData.Language;
        set
        {
            SettingsParametersData.Language = value;
            GlobalEventsUI.ChangeLanguage?.Invoke();
        }
    }

    public static void SaveSetting() => GlobalSave.SaveSettingParameters(SettingsParametersData);
}