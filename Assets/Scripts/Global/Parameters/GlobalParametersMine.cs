public struct DrillParametersData
{
    public int DrillLevel;
    public float DrillFuelTank;
    public int DrillDamage;
    public int FuelTankShopIndex;
    public int DamageShopIndex;
    public int MaxUpdateIndex;
}

public struct BlockParametersData
{
    public int Health;
    public float PenetrationCosts;
}

public static class GlobalParametersMine 
{
    public static DrillParametersData DrillParameters = new DrillParametersData();
}
