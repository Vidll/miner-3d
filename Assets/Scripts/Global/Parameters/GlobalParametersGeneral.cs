public static class GlobalParametersGeneral
{
	public static GlobalGameState CurrentGameState = GlobalGameState.OnStartMenu;

    public enum GlobalGameState
	{
		NONE = 0,
		OnStartMenu,
		OnWorld,
		OnMine
	}
}
