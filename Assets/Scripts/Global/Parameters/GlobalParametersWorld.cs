public struct BuildParametersData
{
    public int Level;
}

public struct WarehouseParametersData
{
    public int MaxSizeWarehouse;
    public int[] CurrentResources;
}

public static class GlobalParametersWorld 
{
    public static BuildParametersData BuildParameters = new BuildParametersData();
    public static WarehouseParametersData WarehouseParametersData = new WarehouseParametersData();
}
