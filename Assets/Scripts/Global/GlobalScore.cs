public struct ScoreData
{
    public int Money;
    public int CoalMaterials;
    public int CopperMaterials;
    public int IronMaterials;
    public int GoldMaterials;
    public int RedStoneMaterials;
    public int EmeraldMaterials;
    public int DiamondMaterials;
}

public struct ResourcesData
{
    public int CoalResouces;
    public int CopperResources;
    public int IronResources;
    public int GoldResources;
    public int RedStoneResources;
    public int EmeraldResources;
    public int DiamondResouces;
}

public static class GlobalScore
{
    private static ScoreData MyScoreData = new ScoreData();
    private static ResourcesData MyResourcesData = new ResourcesData();
    public static void SaveScore() => GlobalSave.SaveScore(MyScoreData, MyResourcesData);

    public static int Money
	{
        get => MyScoreData.Money;
		set
        {
            MyScoreData.Money += value;
            GlobalEventsUI.SetMoney?.Invoke();
        }
	}

    //Resources
    public static int CoalResources 
    {
        get => MyResourcesData.CoalResouces;
        set 
        {
            if (value <= 0)
                return;
            MyResourcesData.CoalResouces += value;
            GlobalEventsUI.SetResouces?.Invoke(1);
        } 
    }

    public static int CopperResources
	{
        get => MyResourcesData.CopperResources;
        set
		{
            if (value <= 0)
                return;
            MyResourcesData.CopperResources += value;
            GlobalEventsUI.SetResouces?.Invoke(2);
        }
    }

    public static int IronResources
    {
        get => MyResourcesData.IronResources;
        set
        {
            if (value <= 0)
                return;
            MyResourcesData.IronResources += value;
            GlobalEventsUI.SetResouces?.Invoke(3);
        }
    }

    public static int GoldResources
    {
        get => MyResourcesData.GoldResources;
        set
        {
            if (value <= 0)
                return;
            MyResourcesData.GoldResources += value;
            GlobalEventsUI.SetResouces?.Invoke(4);
        }
    }

    public static int RedStoneResources
    {
        get => MyResourcesData.RedStoneResources;
        set
        {
            if (value <= 0)
                return;
            MyResourcesData.RedStoneResources += value;
            GlobalEventsUI.SetResouces?.Invoke(5);
        }
    }

    public static int EmeraldResources
    {
        get => MyResourcesData.EmeraldResources;
        set
        {
            if (value <= 0)
                return;
            MyResourcesData.EmeraldResources += value;
            GlobalEventsUI.SetResouces?.Invoke(6);
        }
    }

    public static int DiamondResouces
    {
        get => MyResourcesData.DiamondResouces;
        set
        {
            if (value <= 0)
                return;
            MyResourcesData.DiamondResouces += value;
            GlobalEventsUI.SetResouces?.Invoke(7);
        }
    }

    //Materials
    public static int CoalMaterials
	{
        get => MyScoreData.CoalMaterials;
		set
		{
            if (value <= 0)
                return;
            MyScoreData.CoalMaterials += value;
            GlobalEventsUI.SetMaterials?.Invoke(1);
		}
	}

    public static int CopperMaterials
    {
        get => MyScoreData.CopperMaterials;
        set
        {
            if (value <= 0)
                return;
            MyScoreData.CopperMaterials += value;
            GlobalEventsUI.SetMaterials?.Invoke(2);
        }
    }

    public static int IronMaterials
    {
        get => MyScoreData.IronMaterials;
        set
        {
            if (value <= 0)
                return;
            MyScoreData.IronMaterials += value;
            GlobalEventsUI.SetMaterials?.Invoke(3);
        }
    }

    public static int GoldMaterials
    {
        get => MyScoreData.GoldMaterials;
        set
        {
            if (value <= 0)
                return;
            MyScoreData.GoldMaterials += value;
            GlobalEventsUI.SetMaterials?.Invoke(4);
        }
    }

    public static int RedStoneMaterials
    {
        get => MyScoreData.RedStoneMaterials;
        set
        {
            if (value <= 0)
                return;
            MyScoreData.RedStoneMaterials += value;
            GlobalEventsUI.SetMaterials?.Invoke(5);
        }
    }

    public static int EmeraldMaterials
    {
        get => MyScoreData.EmeraldMaterials;
        set
        {
            if (value <= 0)
                return;
            MyScoreData.EmeraldMaterials += value;
            GlobalEventsUI.SetMaterials?.Invoke(6);
        }
    }

    public static int DiamondMaterials
    {
        get => MyScoreData.DiamondMaterials;
        set
        {
            if (value <= 0)
                return;
            MyScoreData.DiamondMaterials += value;
            GlobalEventsUI.SetMaterials?.Invoke(7);
        }
    }
}