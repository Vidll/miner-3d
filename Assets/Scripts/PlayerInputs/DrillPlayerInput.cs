using UnityEngine;

public class DrillPlayerInput : MonoBehaviour
{
	private Drill _drill;
	private bool _avalible = false;

	public void EnableInput()
	{
		_drill = GetComponent<Drill>();
		_avalible = true;
	}

	public void DisableInput()
	{
		_avalible = false;
	}

	private void Update()
	{
		if (!_avalible)
			return;

		if (Input.GetMouseButtonDown(0))
			_drill.EnableCustomGravity(Input.mousePosition);
		else if (Input.GetMouseButton(0))
			_drill.MovedDrill(Input.mousePosition);
		else if (Input.GetMouseButtonUp(0))
			_drill.DisabeCustomGravity();
	}
}
