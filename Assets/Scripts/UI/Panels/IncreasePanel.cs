using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class IncreasePanel : MonoBehaviour
{
    [SerializeField] private Text _mainText;
    [SerializeField] private Text _costText;
    [SerializeField] private Text _stateText;
    [SerializeField] private Button _increaseButton;

    public void SetButtonAction(UnityAction action)
	{
        if (_increaseButton)
            _increaseButton.onClick.AddListener(action);
        else
            Debug.Log(gameObject.name + " dont set action button");
	}

    public void SetText(string cost, string currentState, string nextState, string main)
	{
        _increaseButton.enabled = true;
        _mainText.text = main;
        _costText.text = cost;
        if(_stateText)
            _stateText.text = currentState + " + " + nextState;
    }

    public void SetText(string cost, string currentState, string nextState)
	{
        _increaseButton.enabled = true;
        _costText.text = cost;
        if (_stateText)
            _stateText.text = currentState + " + " + nextState;
    }

    public void SetMaxState()
	{
        _increaseButton.enabled = false;
        _costText.text = "MAX";
	}
}
