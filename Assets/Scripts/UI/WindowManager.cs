using UnityEngine;

public class WindowManager : MonoBehaviour
{
	[SerializeField] private Window[] _windows;

	private void Start()
	{
		_windows = GetComponentsInChildren<Window>();
		GlobalEventsUI.OpenWindow += OpenWindow;
		GlobalEventsUI.SearchAndOpenWindow += SearchWindowsAndOpenWindow;
		GlobalEventsGeneral.GoToMine += CloseAllWindows;
	}

	public void OpenWindow(int index)
	{
		CloseAllWindows();
		foreach (var window in _windows)
			if (window.GetIndex == index)
			{
				window.OpenWindow();
				return;
			}
			else
				Debug.LogError("Index don`t find");
	}

	public void SearchWindowsAndOpenWindow(int index)
	{
		_windows = FindObjectsOfType<Window>();
		OpenWindow(index);
	}

	public void CloseAllWindows()
	{
		foreach (var window in _windows)
			window.CloseWindow();
	}
}
