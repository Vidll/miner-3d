using UnityEngine;
using UnityEngine.UI;

public class GeneralInterface : UIWindow
{
    [SerializeField] private Text _moneyText;
	[SerializeField] private Button _settingButton;

	public override void Init()
	{
		base.Init();
		OpenWindow();
		GlobalEventsUI.SetMoney += SetMoneyText;

		if (_settingButton)
			_settingButton.onClick.AddListener(OpenSettingWindow);

		_moneyText.text = "Money:" + GlobalScore.Money;
	}

	public override void OpenWindow()
	{
		base.OpenWindow();
	}

	public override void CloseWindow()
	{
		base.CloseWindow();
	}

	private void OpenSettingWindow()
	{

	}

	private void SetMoneyText() => _moneyText.text = "Money: " + GlobalScore.Money;
}