using UnityEngine;
using UnityEngine.UI;

public class MineInterface : UIWindow
{
	[SerializeField] private GameObject _stuckWindow;
	[SerializeField] private Text _drillFuelTankText;
	[SerializeField] private Button _backToWorldButton;

	public override void Init()
	{
		base.Init();
		GlobalEventsGeneral.GoToMine += OpenWindow;
		GlobalEventsGeneral.GoToWorld += CloseWindow;
		GlobalEventsMine.ChangeDrillFuelTank += SetDrillFuelTank;
		GlobalEventsMine.StuckStart += OpenStuckWindow;

		if (_backToWorldButton)
			_backToWorldButton.onClick.AddListener(OnClickBackToWorld);
	}

	public override void OpenWindow()
	{
		base.OpenWindow();
		SetDrillFuelTank(GlobalParametersMine.DrillParameters.DrillFuelTank);
	}

	public override void CloseWindow()
	{
		if (_stuckWindow.gameObject.activeSelf)
			_stuckWindow.SetActive(false);
		base.CloseWindow();
	}

	private void OnClickBackToWorld()
	{
		GlobalEventsGeneral.GoToWorldMethod();
	}

	private void OpenStuckWindow()
	{
		GlobalEventsMine.StuckStart -= OpenStuckWindow;
		GlobalEventsMine.BreackOutStop += CloseStuckWindow;
		_stuckWindow.SetActive(true);
	}

	private void CloseStuckWindow()
	{
		GlobalEventsMine.StuckStart += OpenStuckWindow;
		GlobalEventsMine.BreackOutStop -= CloseStuckWindow;
		_stuckWindow.SetActive(false);
	}

	private void SetDrillFuelTank(float value)
	{
		_drillFuelTankText.text = value.ToString();
	}
}