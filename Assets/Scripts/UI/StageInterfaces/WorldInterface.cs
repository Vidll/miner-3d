using UnityEngine;

public class WorldInterface : UIWindow
{
	[SerializeField] private WorldPlayerGamepadInput _worldPlayerGamepadInput;

	public override void Init()
	{
		base.Init();
		OpenWindow();

		_worldPlayerGamepadInput = GetComponentInChildren<WorldPlayerGamepadInput>();
		_worldPlayerGamepadInput.Init();
	}

	public override void OpenWindow()
	{
		base.OpenWindow();
	}
	public override void CloseWindow()
	{
		base.CloseWindow();
	}
}
