using UnityEngine;
using UnityEngine.UI;

public class TestingInterface : UIWindow
{
	[SerializeField] private bool _show = true;
    [SerializeField] private Button _changeGame;
	[SerializeField] private Button _openDrillShopButton;
	[SerializeField] private Text[] _resourcesText;
	[SerializeField] private Text[] _materialText;

	public override void Init()
	{
		if (!_show)
			return;

		base.Init();
		OpenWindow();

		SetResourcesText(0);
		SetMaterialText(0);

		if (_changeGame)
			_changeGame.onClick.AddListener(ChangeGameButton);
		if (_openDrillShopButton)
			_openDrillShopButton.onClick.AddListener(OpenDrillShop);

		GlobalEventsUI.SetMaterials += SetMaterialText;
		GlobalEventsUI.SetResouces += SetResourcesText;
	}

	public override void OpenWindow()
	{
		base.OpenWindow();
	}

	public override void CloseWindow()
	{
		base.CloseWindow();
	}

	private void SetResourcesText(int i)
	{
		_resourcesText[0].text = "Coal R.: " + GlobalScore.CoalResources;
		_resourcesText[1].text = "Copper R.: " + GlobalScore.CopperResources;
		_resourcesText[2].text = "Iron R.: " + GlobalScore.IronResources;
		_resourcesText[3].text = "Gold R.: " + GlobalScore.GoldResources;
		_resourcesText[4].text = "Red stone R.: " + GlobalScore.RedStoneResources;
		_resourcesText[5].text = "Emeral R.: " + GlobalScore.EmeraldResources;
		_resourcesText[6].text = "Diamond R.: " + GlobalScore.DiamondResouces;
	}

	private void SetMaterialText(int i)
	{
		_materialText[0].text = "Coal M.:" + GlobalScore.CoalMaterials;
		_materialText[1].text = "Copper M.:" + GlobalScore.CopperMaterials;
		_materialText[2].text = "Iron M.:" + GlobalScore.IronMaterials;
		_materialText[3].text = "Gold M.:" + GlobalScore.GoldMaterials;
		_materialText[4].text = "Res stone M.:" + GlobalScore.RedStoneMaterials;
		_materialText[5].text = "Emeral M.:" + GlobalScore.EmeraldMaterials;
		_materialText[6].text = "Diamong M.:" + GlobalScore.DiamondMaterials;
	}

	private void OpenDrillShop()
	{
		GlobalEventsUI.OpenWindow.Invoke(1);
	}

	private void ChangeGameButton()
	{
		if (GlobalParametersGeneral.CurrentGameState == GlobalParametersGeneral.GlobalGameState.OnWorld)
		{
			GlobalEventsGeneral.GoToMineMethod();
		}
		else if(GlobalParametersGeneral.CurrentGameState == GlobalParametersGeneral.GlobalGameState.OnMine)
		{
			GlobalEventsGeneral.GoToWorldMethod();
		}
		else if (GlobalParametersGeneral.CurrentGameState == GlobalParametersGeneral.GlobalGameState.OnStartMenu)
		{
			GlobalEventsGeneral.GoToWorldFromStartMenuMethod();
		}
	}
}