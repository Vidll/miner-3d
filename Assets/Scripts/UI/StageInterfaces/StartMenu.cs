using UnityEngine;
using UnityEngine.UI;

public class StartMenu : MonoBehaviour
{
	[SerializeField] private GameObject _window;
	[SerializeField] private Button _playButton;
	[SerializeField] private Button _settingButton;
	[SerializeField] private Button _exitButton;

	[SerializeField] private UIWindow[] _uiWindows;

	private void Start()
	{
		_uiWindows = FindObjectsOfType<UIWindow>();
		OpenWindow();

		if (_playButton)
			_playButton.onClick.AddListener(OnClickStartGame);
		if (_settingButton)
			_settingButton.onClick.AddListener(OnClickOpenSettingWindow);
	}

	[ContextMenu("OnClickStartGame")]
	public void OnClickStartGame()
	{
		foreach(UIWindow window in _uiWindows)
			window.Init();
		GlobalEventsGeneral.GoToWorldFromStartMenuMethod();
		CloseWindow();
	}

	public void OnClickOpenSettingWindow()
	{

	}

	private void OpenWindow() 
	{
		_window.SetActive(true);
	}

	private void CloseWindow()
	{
		_window.SetActive(false);
	}
}