using UnityEngine;

public class DrillUpgradeWindow : Window
{
	[Header("Price")]
	[SerializeField] private int _levelBuyPrice;
	[SerializeField] private int _increaseBuyPrice;
	[Header("Setting")]
	[SerializeField] private int _maxUpdateIndexSteep = 5;
	[SerializeField] private int _valueUpIncreaseDivision = 4;
	[Header("Panels")]
	[SerializeField] private IncreasePanel _increaseDamagePanel;
	[SerializeField] private IncreasePanel _increaseFuelTankPanel;
	[SerializeField] private IncreasePanel _increaseLevelPanel;

	[ContextMenu("OpenWindow")]
	public override void OpenWindow()
	{
		base.OpenWindow();
		UpdatePanels();
	}

	[ContextMenu("CoseWindow")]
	public override void CloseWindow()
	{
		base.CloseWindow();
	}

	private void PrintInfo()
	{
		print("Level: " + GlobalParametersMine.DrillParameters.DrillLevel);
		print("Index: " + GlobalParametersMine.DrillParameters.DamageShopIndex + " Damage: " + GlobalParametersMine.DrillParameters.DrillDamage);
		print("Index: " + GlobalParametersMine.DrillParameters.FuelTankShopIndex + " Fuel: " + GlobalParametersMine.DrillParameters.DrillFuelTank);
		print("Max upgrade: " + GlobalParametersMine.DrillParameters.MaxUpdateIndex);
	}

	[ContextMenu("Level up")]
	public void DrillLevelUp()
	{
		if (GlobalScore.Money >= GlobalParametersMine.DrillParameters.DrillLevel * _levelBuyPrice)
		{
			GlobalScore.Money = -(GlobalParametersMine.DrillParameters.DrillLevel * _levelBuyPrice);
			
			GlobalParametersMine.DrillParameters.DrillLevel++;
			GlobalParametersMine.DrillParameters.MaxUpdateIndex += _maxUpdateIndexSteep;
			UpdatePanels();
		}
	}

	[ContextMenu("Damage up")]
	public void DrillDamageUp()
	{
		if (GlobalScore.Money >= GlobalParametersMine.DrillParameters.DamageShopIndex * _increaseBuyPrice)
		{
			GlobalScore.Money = -(GlobalParametersMine.DrillParameters.DamageShopIndex * _increaseBuyPrice);

			GlobalParametersMine.DrillParameters.DamageShopIndex++;
			GlobalParametersMine.DrillParameters.DrillDamage += Mathf.CeilToInt((float)GlobalParametersMine.DrillParameters.DrillDamage / _valueUpIncreaseDivision);
			UpdateDrillDamage();
		}
		if (GlobalParametersMine.DrillParameters.DamageShopIndex >= GlobalParametersMine.DrillParameters.MaxUpdateIndex)
			SetDrillDamageMaximum();
	}

	[ContextMenu("Fuel Tank up")]
	public void DrillFuelTankUp()
	{
		if (GlobalScore.Money >= GlobalParametersMine.DrillParameters.FuelTankShopIndex * _increaseBuyPrice)
		{
			GlobalScore.Money = -(GlobalParametersMine.DrillParameters.FuelTankShopIndex * _increaseBuyPrice);

			GlobalParametersMine.DrillParameters.FuelTankShopIndex++;
			GlobalParametersMine.DrillParameters.DrillFuelTank += Mathf.Ceil(GlobalParametersMine.DrillParameters.DrillFuelTank / _valueUpIncreaseDivision);

			UpdateDrillFuelTank();
		}
		if (GlobalParametersMine.DrillParameters.FuelTankShopIndex >= GlobalParametersMine.DrillParameters.MaxUpdateIndex)
			SetDrillFuelTankMaximum();
	}

	public override void SetContentInStart()
	{
		base.SetContentInStart();
		_increaseDamagePanel.SetButtonAction(DrillDamageUp);
		_increaseFuelTankPanel.SetButtonAction(DrillFuelTankUp);
		_increaseLevelPanel.SetButtonAction(DrillLevelUp);
	}

	private void UpdatePanels()
	{
		UpdateDrillDamage();
		UpdateDrillFuelTank();
		UpdateLevelUp();
	}

	private void UpdateDrillFuelTank()
	{
		_increaseFuelTankPanel.SetText(
			(GlobalParametersMine.DrillParameters.FuelTankShopIndex * _increaseBuyPrice).ToString(),
			GlobalParametersMine.DrillParameters.DrillFuelTank.ToString(), (GlobalParametersMine.DrillParameters.DrillFuelTank + Mathf.Ceil(GlobalParametersMine.DrillParameters.DrillFuelTank / _valueUpIncreaseDivision)).ToString(),
			  "Increase drill fuel tank"); // TODO: �������� ����� �����
	}

	private void UpdateDrillDamage()
	{
		_increaseDamagePanel.SetText(
			(GlobalParametersMine.DrillParameters.DamageShopIndex * _increaseBuyPrice).ToString(),
			GlobalParametersMine.DrillParameters.DrillDamage.ToString(),
			(GlobalParametersMine.DrillParameters.DrillDamage + Mathf.CeilToInt((float)GlobalParametersMine.DrillParameters.DrillDamage / _valueUpIncreaseDivision)).ToString(),
			"Increase drill damage"); // TODO: �������� ����� �����
	}

	private void UpdateLevelUp()
	{
		_increaseLevelPanel.SetText(
			(GlobalParametersMine.DrillParameters.DrillLevel * _levelBuyPrice).ToString(),
			GlobalParametersMine.DrillParameters.DrillLevel.ToString(),
			(GlobalParametersMine.DrillParameters.DrillLevel + 1).ToString(),
			"Increse drill level"); // TODO: �������� ����� �����
	}

	private void SetDrillFuelTankMaximum() => _increaseFuelTankPanel.SetMaxState();
	private void SetDrillDamageMaximum() => _increaseDamagePanel.SetMaxState();
}
