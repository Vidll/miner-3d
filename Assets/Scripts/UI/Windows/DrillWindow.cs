using UnityEngine;
using UnityEngine.UI;

public class DrillWindow : MonoBehaviour
{
	[SerializeField] private GameObject _windows;
	[SerializeField] private Image _imageSlider;
	[SerializeField] private Image _imageFuelIsEmpty;

	private void OpenWindow()
	{
		_windows.SetActive(true);
	}

	public void CloseWindow()
	{
		_windows.SetActive(false);
	}

	public void EnableStuck()
	{
		_imageSlider.fillAmount = 0.0f;
		GlobalEventsMine.BreackOut += SetFillAmount;
		_imageSlider.gameObject.SetActive(true);
		OpenWindow();
	}

	public void DisableStuck()
	{
		GlobalEventsMine.BreackOut -= SetFillAmount;
		GlobalEventsMine.BreackOutStop?.Invoke();
		_imageSlider.gameObject.SetActive(false);
		CloseWindow();
	}
	
	private void SetFillAmount(float value)
	{
		_imageSlider.fillAmount += value;
		if (_imageSlider.fillAmount >= 1)
			DisableStuck();
	}

	public void FuelIsEmptyEnable()
	{
		_imageFuelIsEmpty.gameObject.SetActive(true);
		OpenWindow();
	}

	public void FuelIsEmptyDisable()
	{
		_imageFuelIsEmpty.gameObject.SetActive(false);
		CloseWindow();
	}
}