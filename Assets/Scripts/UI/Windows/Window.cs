using UnityEngine;
using UnityEngine.UI;

public abstract class Window : MonoBehaviour
{
	[SerializeField] private int _windowIndex;
	[SerializeField] private GameObject _window;
	[SerializeField] private Button _closeButton;

	public int GetIndex { get => _windowIndex; }

	private void Start()
	{
		SetContentInStart();
	}

	public virtual void SetContentInStart()
	{
		if(_closeButton)
			_closeButton.onClick.AddListener(CloseWindow);
	}

	public virtual void OpenWindow()
	{
		if (_window)
			_window.SetActive(true);
	}

	public virtual void CloseWindow()
	{
		if(_window)
			_window.SetActive(false);
	}
}
