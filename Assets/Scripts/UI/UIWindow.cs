using UnityEngine;

public abstract class UIWindow : MonoBehaviour
{
    public GameObject Window;

    public virtual void Init() { }

    public virtual void OpenWindow()
	{
        Window.SetActive(true);
	}

    public virtual void CloseWindow()
	{
        Window.SetActive(false);
	}
}
