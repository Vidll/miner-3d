using UnityEngine;

public class DrillGenerator : MonoBehaviour
{
	[SerializeField] private Drill _drillPrefab;
	[SerializeField] private Transform _spawnPoint;
	[SerializeField] private CameraFollower _cameraFollower;

	public Drill CurrentDrill;

	private void Start()
	{
		GlobalEventsGeneral.GoToMine += CreateDrill;
		GlobalEventsGeneral.GoToWorld += DestroyDrill;
	}

	private void CreateDrill()
	{
		if (!_drillPrefab)
			return;

		CurrentDrill = Instantiate(_drillPrefab, _spawnPoint);
		CurrentDrill.transform.parent = transform;
		CurrentDrill.Init();
		_cameraFollower.FollowObject = CurrentDrill.transform;
		_cameraFollower.EnableFollowing();
	}

	private void DestroyDrill()
	{
		if(CurrentDrill)
			Destroy(CurrentDrill.gameObject);
	}
}
