using UnityEngine;

public class Drill : MonoBehaviour
{
	[SerializeField] private float _attackTimeDeley = .5f;
	[SerializeField] private float _movedSpeed = 10;
	[SerializeField] private float _customGravityForce;
	[SerializeField] private float _touchRange;

	[SerializeField] private int _heightDeleteUpLines = 10;
	[SerializeField] private int _stuckPrecent = 50;

	private Rigidbody _rb;
	private Transform _myTransform;
	private ParticleSystem _particle;
	private Block _currentBlock;
	private DrillWindow _drillWindow;

	private DrillParametersData _myParameters;
	private MovedInputState _currentMovedInputState;

	private Vector3 _startPos;
	private Vector3 _touchPos;

	private float _currentAttackTimeDeley = 0;
	private float _pastPositionX;

	private int _heightsValue = 0;

	private bool _fuelIsEmpty = false;
	private bool _stuckActivate = false;

	public DrillParametersData DrillParameters { get => _myParameters; }

    public void Init()
	{
		_myParameters = GlobalParametersMine.DrillParameters;
		GetComponent<DrillPlayerInput>().EnableInput();
		_rb = GetComponent<Rigidbody>();
		_myTransform = GetComponent<Transform>();
		_particle = GetComponent<ParticleSystem>();
		_drillWindow = GetComponentInChildren<DrillWindow>();

		//_myParameters.DrillFuelTank = 100;
		//_myParameters.DrillDamage = 3;
	}

	private void Update()
	{
		_currentAttackTimeDeley -= Time.deltaTime;
		if (_stuckActivate && Input.GetMouseButtonDown(0))
			GlobalEventsMine.BreackOut.Invoke(0.35f);
	}

	private void OnTriggerStay(Collider other)
	{
		if(other.TryGetComponent(out Block block)
			&& _currentAttackTimeDeley <= 0
			&& !_fuelIsEmpty)
		{
			_myParameters.DrillFuelTank -= block.PenetrationCosts;
			_currentAttackTimeDeley = _attackTimeDeley;

			if (!_stuckActivate)
			{
				if (Random.Range(0, _stuckPrecent) == 0)
					GetStuckEnable(block);
				if (block)
					block.TakeDamage(_myParameters.DrillDamage);
			}

			GlobalEventsMine.ChangeDrillFuelTank?.Invoke(_myParameters.DrillFuelTank);
			if (_myParameters.DrillFuelTank <= 0)
				FuelIsEmptu();
		}
	}

	private void GetStuckEnable(Block block)
	{
		GlobalEventsMine.BreackOutStop += GetStuckDisable;
		_drillWindow.EnableStuck();
		_stuckActivate = true;
		_currentBlock = block;
		GlobalEventsMine.StuckStart?.Invoke();
	}

	private void GetStuckDisable()
	{
		GlobalEventsMine.BreackOutStop -= GetStuckDisable;
		if(_currentBlock)
			_currentBlock.TakeDamage(1000000);

		_stuckActivate = false;
		_currentBlock = null;
	}

	private void FuelIsEmptu()
	{
		if (_stuckActivate)
			_drillWindow.DisableStuck();
		_drillWindow.FuelIsEmptyEnable();
		_fuelIsEmpty = true;
		// ��������� �������� ��������� �������
	}

	public void EnableCustomGravity(Vector3 startPos)
	{
		_startPos = startPos;
		_pastPositionX = startPos.x;
		_currentMovedInputState = MovedInputState.Stay;
	}

	public void DisabeCustomGravity()
	{
		_currentMovedInputState = MovedInputState.Stay;
		_startPos = Vector3.zero;
		_touchPos = Vector3.zero;
		_rb.velocity = Vector3.zero;
	}

	public void MovedDrill(Vector3 touch)
	{
		if (_stuckActivate)
			return;
		_touchPos = touch;

		if (_currentMovedInputState == MovedInputState.Stay)
		{
			if(_touchPos.x < _startPos.x)
				_currentMovedInputState = MovedInputState.MoveLeft;
			else if (_touchPos.x > _startPos.x)
				_currentMovedInputState = MovedInputState.MoveRight;
		}
		else if (_currentMovedInputState == MovedInputState.MoveLeft)
		{
			if(_pastPositionX < _touchPos.x)
			{
				_currentMovedInputState = MovedInputState.MoveRight;
				_startPos = _touchPos;
			}
		}
		else if(_currentMovedInputState == MovedInputState.MoveRight)
		{
			if (_pastPositionX > _touchPos.x)
			{
				_currentMovedInputState = MovedInputState.MoveLeft;
				_startPos = _touchPos;
			}
		}

		var value = (_touchPos.x - _startPos.x) / 100;

		if (value > _touchRange)
			value = _touchRange;
		else if (value < -_touchRange)
			value = -_touchRange;

		_pastPositionX = _touchPos.x;

		_rb.velocity = new Vector3(value * _movedSpeed, -1 * _customGravityForce, 0);

		if (_myTransform.position.y < _heightsValue)
			MovingDown();
	}

	private void MovingDown()
	{
		_heightsValue--;
		GlobalEventsMine.CreateMineLine?.Invoke();
		if (_myTransform.position.y < _heightDeleteUpLines)
			GlobalEventsMine.DestroyMineLine?.Invoke();
	}

	private enum MovedInputState
	{
		NONE = 0,
		Stay,
		MoveLeft,
		MoveRight,
	}
}
