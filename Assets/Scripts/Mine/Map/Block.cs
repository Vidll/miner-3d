using UnityEngine;

public class Block : MonoBehaviour
{
    private BlockParametersData _blockParameters = new BlockParametersData();
    private ResourcesData _resources = new ResourcesData();
    private Material _material;
    private int _index;

    public int Index { get => _index; }
    public float PenetrationCosts { get => _blockParameters.PenetrationCosts; }

    public void Init(int index, Material newMaterial, ResourcesData resources, BlockParametersData blockData)
	{
        _blockParameters = blockData;
        _index = index;
        _resources = resources;
        SetMaterial(newMaterial);
    }

	public void SetMaterial(Material newMaterial)
	{
        _material = newMaterial;
        gameObject.GetComponent<MeshRenderer>().material = _material;
	}
    
    public void TakeDamage(int damage) 
    {
        _blockParameters.Health -= damage;
        if (_blockParameters.Health <= 0)
            DeleteBlock();
    }

    private void DeleteBlock()
	{
        GlobalEventsMine.DestroyBlock?.Invoke();
        SetResources();
        DestroyBlock();
	}

    private void SetResources()
	{
        GlobalScore.CoalResources = _resources.CoalResouces;
        GlobalScore.CopperResources = _resources.CopperResources;
        GlobalScore.IronResources = _resources.IronResources;
        GlobalScore.GoldResources = _resources.GoldResources;
        GlobalScore.RedStoneResources = _resources.RedStoneResources;
        GlobalScore.EmeraldResources = _resources.EmeraldResources;
        GlobalScore.DiamondResouces = _resources.DiamondResouces;
    }
        
    public void DestroyBlock() => Destroy(gameObject);
}
