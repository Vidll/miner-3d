using System.Collections.Generic;
using UnityEngine;

public class MineGenerator : MonoBehaviour
{
	[SerializeField] private Block _blockPrefab;

	[SerializeField] private Vector3 _upPoint;
	[SerializeField] private Vector3 _downPoint;

	[SerializeField] private Vector2[] _blockSteep;
	[SerializeField] private Vector2[] _distanceBetweenBlocks;

	[Space][Header("Testing parameters (Dont touch)")]
	[SerializeField] private Vector3 _currentPos;
	[SerializeField] private List<Block> _currentBlocks = new List<Block>();

	private Transform MyTransform;
	private BlockGenerator _blockGenerator;

	private int _blockIndex = 0;
	private int _lineLength = 0;

	private void Start()
	{
		GlobalEventsGeneral.GoToMine += OpenMine;
		MyTransform = transform;
		_blockGenerator = GetComponent<BlockGenerator>();
	}

	private void OpenMine()
	{
		GenerateMine();
		GlobalEventsGeneral.GoToMine -= OpenMine;
		GlobalEventsMine.CreateMineLine += CreateLine;
		GlobalEventsMine.DestroyMineLine += ClearLine;
		GlobalEventsGeneral.GoToWorld += CloseMine;
	}

	private void CloseMine()
	{
		ClearMine();
		GlobalEventsMine.CreateMineLine -= CreateLine;
		GlobalEventsMine.DestroyMineLine -= ClearLine;
		GlobalEventsGeneral.GoToWorld -= CloseMine;
		GlobalEventsGeneral.GoToMine += OpenMine;
	}

	private void GenerateMine()
	{
		_currentPos = _upPoint;
		while (_currentPos.y >= _downPoint.y)
		{
			while (_currentPos.x <= _downPoint.x)
				CreateBlock();
			_currentPos.x = _upPoint.x;
			_currentPos.y -= 1;
		}
		_lineLength = GetLineLength();
	}

	private void ClearMine()
	{
		foreach (var block in _currentBlocks)
		{
			if(block)
				Destroy(block.gameObject);
		}
		_currentBlocks.Clear();
	}

	[ContextMenu("Create Line")]
	private void CreateLine()
	{
		if (_currentPos == Vector3.zero)
			return;

		while (_currentPos.x <= _downPoint.x)
			CreateBlock();
		_currentPos.x = _upPoint.x;
		_currentPos.y -= 1;
	}

	[ContextMenu("Clear Line")]
	private void ClearLine()
	{
		for(int i = 0; i < _lineLength; i++)
		{
			if (_currentBlocks[0])
				_currentBlocks[0].DestroyBlock();
			_currentBlocks.RemoveAt(0);
		}
	}

	private void CreateBlock()
	{
		var block = Instantiate(_blockPrefab);
		Transform blockTransform = block.GetComponent<Transform>();
		blockTransform.parent = MyTransform;
		blockTransform.localPosition = _currentPos;

		_blockGenerator.GetBlock(_currentPos.y);
		block.Init(_blockIndex, _blockGenerator.GetBlockMaterial, _blockGenerator.GetBlockResourcesData, _blockGenerator.GetBlockParametersData);

		_currentBlocks.Add(block);
		_currentPos.x += 1;
	}

	private int GetLineLength()
	{
		int length = 0;
		while (_currentPos.x <= _downPoint.x)
		{
			length++;
			_currentPos.x += 1;
		}
		_currentPos.x = _upPoint.x;
		return length;
	}
}
