using UnityEngine;

public class BlockGenerator : MonoBehaviour
{
	[SerializeField] private Material[] _materials = new Material[8];
	[SerializeField] private int[] _deepRange = new int[6];
	[SerializeField] private int[] _blockHeath = new int[8];
	[SerializeField] private float[] _blockPenetrationCosts = new float[8];

	private Material _blockMaterial;
	private ResourcesData _blockResourcesData;
	private BlockParametersData _blockParametersData;

	private float _randomValue;

	public Material GetBlockMaterial { get => _blockMaterial; } 
	public ResourcesData GetBlockResourcesData { get => _blockResourcesData; }
	public BlockParametersData GetBlockParametersData { get => _blockParametersData; }

    public void GetBlock(float deep)
	{
		_blockResourcesData = new ResourcesData();
		_blockParametersData = new BlockParametersData();
		_randomValue = Random.Range(0.0f,10.0f);

		if (_randomValue > 9.5f)
			GetResourceBlock(deep);
		else
		{
			_blockMaterial = _materials[0];
			_blockParametersData.Health = _blockHeath[0];
			_blockParametersData.PenetrationCosts = _blockPenetrationCosts[0];
		}
	}

	private void GetResourceBlock(float deep)
	{
		if (deep < _deepRange[0])
		{
			_blockMaterial = _materials[2];
			_blockResourcesData.CopperResources = 1;
			_blockParametersData.Health = _blockHeath[2];
			_blockParametersData.PenetrationCosts = _blockPenetrationCosts[2];
		}
		else if (deep < _deepRange[1])
		{
			_blockMaterial = _materials[3];
			_blockResourcesData.IronResources = 1;
			_blockParametersData.Health = _blockHeath[3];
			_blockParametersData.PenetrationCosts = _blockPenetrationCosts[3];
		}
		else if (deep < _deepRange[2])
		{
			_blockMaterial = _materials[4];
			_blockResourcesData.GoldResources = 1;
			_blockParametersData.Health = _blockHeath[4];
			_blockParametersData.PenetrationCosts = _blockPenetrationCosts[4];
		}
		else if (deep < _deepRange[3])
		{
			_blockMaterial = _materials[5];
			_blockResourcesData.RedStoneResources = 1;
			_blockParametersData.Health = _blockHeath[5];
			_blockParametersData.PenetrationCosts = _blockPenetrationCosts[5];
		}
		else if (deep < _deepRange[4])
		{
			_blockMaterial = _materials[6];
			_blockResourcesData.EmeraldResources = 1;
			_blockParametersData.Health = _blockHeath[6];
			_blockParametersData.PenetrationCosts = _blockPenetrationCosts[6];
		}
		else if (deep < _deepRange[5])
		{
			_blockMaterial = _materials[7];
			_blockResourcesData.DiamondResouces = 1;
			_blockParametersData.Health = _blockHeath[7];
			_blockParametersData.PenetrationCosts = _blockPenetrationCosts[7];
		}
		else
		{
			_blockMaterial = _materials[1];
			_blockResourcesData.CoalResouces = 1;
			_blockParametersData.Health = _blockHeath[1];
			_blockParametersData.PenetrationCosts = _blockPenetrationCosts[1];
		}
	}
}
