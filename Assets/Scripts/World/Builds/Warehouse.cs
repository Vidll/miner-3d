using UnityEngine;
using UnityEngine.UI;

public class Warehouse : Build
{
	[SerializeField] private Button _updateButton;

	[SerializeField] private Vector3 _resouceOnPointPosition;
	[SerializeField] private Vector3 _resouceOnPointUpperPosition;
	[SerializeField] private ResourcePoint[] _resourcePositions;
	[SerializeField] private GameObject[] _resourcesPrefab;
	private Resource[] _resources;
	public int ResouceCount = 0;

	private void OnEnable()
	{
		if (_updateButton)
			_updateButton.onClick.AddListener(OnClickUpdateBuild);

		SetResourcesArray();
		GlobalEventsUI.SetResouces += SetResources;
	}

	private void SetResourcesArray()
	{
		var i = 0;
		_resources = new Resource[_resourcesPrefab.Length];
		foreach(GameObject obj in _resourcesPrefab)
		{
			_resources[i] = obj.GetComponent<Resource>();
			i++;
		}

	}

	private void SetResources(int index)
	{
		var i = 0;
		foreach(Resource resource in _resources)
		{
			if(index == resource.Index)
			{
				CreateResource(_resourcesPrefab[i]);
				return;
			}
			i++;
		}
	}

	private void CreateResource(GameObject resourcePrefab)
	{
		if (ResouceCount >= GlobalParametersWorld.WarehouseParametersData.MaxSizeWarehouse)
		{
			GlobalEventsWorld.WarehouseFull?.Invoke();
			return;
		}

		var res = Instantiate(resourcePrefab,_resourcePositions[ResouceCount].PointPosition,Quaternion.identity);
		res.transform.parent = _resourcePositions[ResouceCount].MyTransform;
		_resourcePositions[ResouceCount].SetResource();
		ResouceCount++;
	}

	private void OnClickUpdateBuild()
	{
		UpdateBuild();
	}

	public override void OpenInterface()
	{
		base.OpenInterface();
	}

	public override void CloseInterface()
	{
		base.CloseInterface();
	}

	public override void UpdateBuild()
	{
		base.UpdateBuild();
	}
}
