using UnityEngine;
using UnityEngine.UI;

public class MineEntrance : Build
{
	[SerializeField] private Button _goToMineButton;

	private void OnEnable()
	{
		if (_goToMineButton)
			_goToMineButton.onClick.AddListener(OnClickGoToMine);
	}

	public override void OpenInterface()
	{
		base.OpenInterface();
	}

	public override void CloseInterface()
	{
		base.CloseInterface();
	}

	private void OnClickGoToMine()
	{
		GlobalEventsGeneral.GoToMineMethod();
	}
}
