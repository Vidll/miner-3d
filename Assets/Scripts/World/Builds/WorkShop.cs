using UnityEngine;
using UnityEngine.UI;

public class WorkShop : Build
{
	[SerializeField] private Button _openWorkShopButton;

	private void OnEnable()
	{
		if (_openWorkShopButton)
			_openWorkShopButton.onClick.AddListener(OnClickOpenWorkShopWindow);
	}

	public override void OpenInterface()
	{
		base.OpenInterface();
	}

	public override void CloseInterface()
	{
		base.CloseInterface();
	}

	private void OnClickOpenWorkShopWindow()
	{
		GlobalEventsUI.OpenWindow.Invoke(1);
	}
}
