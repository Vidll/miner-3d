using UnityEngine;

public abstract class Build : MonoBehaviour
{
	[SerializeField] private GameObject _buildInterface;

	public BuildParametersData MyBuildParameters;

	private void OnEnable()
	{
		MyBuildParameters = new BuildParametersData();	
	}
	
	private void OnTriggerEnter(Collider other)
	{
		if(other.TryGetComponent(out Player player))
		{
			OpenInterface();
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.TryGetComponent(out Player player))
		{
			CloseInterface();
		}
	}

	public virtual void UpdateBuild()
	{
		MyBuildParameters.Level++;
	}

	public virtual void OpenInterface()
	{
		if(_buildInterface)
			_buildInterface.SetActive(true);
	}

	public virtual void CloseInterface()
	{
		if(_buildInterface)
			_buildInterface.SetActive(false);
	}
}
