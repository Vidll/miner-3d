using UnityEngine;

public class PlayerMover : MonoBehaviour
{
	[SerializeField] private Transform _moveObject;
	[SerializeField] private WorldPlayerGamepadInput _gamepad;
	[SerializeField] private CameraFollower _cameraFollower;
	[SerializeField] private float _moveSpeed = 5;

	private Transform _myTransform;
	private Vector2 _movedVector;
	private bool _move = false;

	private void Start()
	{
		if (!_gamepad)
			_gamepad = FindObjectOfType<WorldPlayerGamepadInput>();

		_myTransform = GetComponent<Transform>();
		GlobalEventsGeneral.ChangeGameState += ChangeStateCameraFollower;
		Subscribe();
	}

	private void Update()
	{
		if(!_move)
			return;
		Moved();
		Rotate();
	}

	private void ChangeStateCameraFollower()
	{
		if(GlobalParametersGeneral.CurrentGameState != GlobalParametersGeneral.GlobalGameState.OnWorld)
		{
			_cameraFollower.EnableFollowing();
		}
		else if (GlobalParametersGeneral.CurrentGameState == GlobalParametersGeneral.GlobalGameState.OnWorld)
		{
			_cameraFollower.DisableFollowing();
		}
	}

	private void Subscribe()
	{
		_gamepad.StartInput += StartInput;
		_gamepad.UpdateInput += UpdateInput;
		_gamepad.EndInput += EndInput;
	}

	private void StartInput()
	{
		_move = true;
	}

	private void UpdateInput()
	{
		_movedVector = _gamepad.GetVector();
	}

	private void EndInput()
	{
		_movedVector = Vector2.zero;
		_move = false;
	}

	private void Moved()
	{
		_myTransform.Translate(new Vector3(_movedVector.x, 0, _movedVector.y) * _moveSpeed * Time.deltaTime);
	}

	private void Rotate()
	{
		Vector3 rotateVec = new Vector3(_movedVector.x, 0, _movedVector.y);
		if(rotateVec != Vector3.zero)
			_moveObject.rotation = Quaternion.LookRotation(rotateVec);
	}
}