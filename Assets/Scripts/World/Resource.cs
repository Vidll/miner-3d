using UnityEngine;

public class Resource : MonoBehaviour
{
    [SerializeField] private int _index;

    public int Index { get => _index; }
}
