using UnityEngine;

public class ResourcePoint : MonoBehaviour
{
	private int Resources = 0;
	public Transform MyTransform;
	public Vector3 PointPosition { get => MyTransform.position; }

	private void OnEnable()
	{
		MyTransform = GetComponent<Transform>();
	}

	public void SetResource()
	{
		Resources++;
	}

	public void GetResource()
	{
		Resources--;
	}
}
