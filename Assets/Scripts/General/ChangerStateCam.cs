using UnityEngine;

public class ChangerStateCam : MonoBehaviour
{
    [SerializeField] private Camera _mineCamera;
    [SerializeField] private Camera _worldCamera;

	private ChangeCamAnim _changeCamAnim;

	private void Start()
	{
		_changeCamAnim = GetComponent<ChangeCamAnim>();
		GlobalEventsGeneral.GoToWorldFromStartMenu += OnClickStart;
		GlobalEventsGeneral.GoToMine += ChangeOnMine;
		GlobalEventsGeneral.GoToWorld += ChangeOnWorld;
	}

	private void OnClickStart()
	{
		GlobalEventsGeneral.GoToWorldFromStartMenu -= OnClickStart;
		_worldCamera.GetComponent<Mover>().StartMove();
	}

	private void ChangeOnWorld()
	{
		_changeCamAnim.StartAnimation(EnableWorldCam);
	}

    private void ChangeOnMine()
	{
		_changeCamAnim.StartAnimation(EnableMineCam);
	}

	private void EnableWorldCam()
	{
		_worldCamera.gameObject.SetActive(true);
		_mineCamera.gameObject.SetActive(false);
	}

	private void EnableMineCam()
	{
		_mineCamera.gameObject.SetActive(true);
		_worldCamera.gameObject.SetActive(false);
	}

	private void Unsubscribe()
	{
		GlobalEventsGeneral.GoToMine -= ChangeOnMine;
		GlobalEventsGeneral.GoToWorld -= ChangeOnWorld;
	}
}
