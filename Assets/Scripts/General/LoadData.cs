using UnityEngine;

public class LoadData : MonoBehaviour
{
	private void Awake()
	{
		Load();
	}

	public void Load()
	{
		SetDefaultData();
	}

	private void SetDefaultData()
	{
		//Drill
		GlobalParametersMine.DrillParameters.DrillLevel = 1;
		GlobalParametersMine.DrillParameters.DrillFuelTank = 100;
		GlobalParametersMine.DrillParameters.DrillDamage = 3;
		GlobalParametersMine.DrillParameters.DamageShopIndex = 1;
		GlobalParametersMine.DrillParameters.FuelTankShopIndex = 1;
		GlobalParametersMine.DrillParameters.MaxUpdateIndex = 5;
		//Setting
		GlobalParametersSetting.SettingsParametersData.MusicActive = true;
		GlobalParametersSetting.SettingsParametersData.SoundActive = true;
		GlobalParametersSetting.SettingsParametersData.MusicVolume = 1;
		GlobalParametersSetting.SettingsParametersData.SoundVolume = 1;
		GlobalParametersSetting.SettingsParametersData.Language = GlobalLanguage.LanguageState.EN;
		//World
		GlobalParametersWorld.WarehouseParametersData.MaxSizeWarehouse = 30;
		//Builds

	}
}
