using UnityEngine;

public class CameraFollower : MonoBehaviour
{
    [SerializeField] private Transform _followObject;
	[SerializeField] private Vector3 _myPos;
	[SerializeField] private Vector3 _movedVectors = Vector3.zero;
	[SerializeField] private float _movedSpeed = 2;

	private Vector3 _startPos;
	private Vector3 _oldPos;
	private bool _available = false;

    public Transform FollowObject { set { _followObject = value; } }

	private void OnDisable()
	{
		if(GlobalParametersGeneral.CurrentGameState == GlobalParametersGeneral.GlobalGameState.OnWorld)
			_oldPos = transform.position;
	}	

	[ContextMenu("Enable")]
    public void EnableFollowing()
	{
		if (GlobalParametersGeneral.CurrentGameState == GlobalParametersGeneral.GlobalGameState.OnWorld)
		{
			GlobalEventsGeneral.GoToWorld += DisableFollowing;
		}
		else if (GlobalParametersGeneral.CurrentGameState == GlobalParametersGeneral.GlobalGameState.OnMine)
		{
			GlobalEventsGeneral.GoToMine += DisableFollowing;
			transform.position = _oldPos;
		}
		_startPos = transform.position;

		if (_followObject)
			_available = true;
	}

	[ContextMenu("Disable")]
	public void DisableFollowing()
	{
		if (GlobalParametersGeneral.CurrentGameState == GlobalParametersGeneral.GlobalGameState.OnMine)
		{
			GlobalEventsGeneral.GoToWorld -= DisableFollowing;
		}
		else if (GlobalParametersGeneral.CurrentGameState == GlobalParametersGeneral.GlobalGameState.OnWorld)
		{
			GlobalEventsGeneral.GoToMine -= DisableFollowing;
		}
		transform.position = _startPos;
		_available = false;
	}

	private void Update()
	{
		if (!_available)
			return;

		transform.position = Vector3.Lerp(
			transform.position,
			new Vector3(_movedVectors.x * _followObject.position.x,
			_movedVectors.y * _followObject.position.y,
			_movedVectors.z * _followObject.position.z) + _myPos,
			_movedSpeed * Time.deltaTime);
	}
}
